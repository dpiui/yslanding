//Require
var gulp = require('gulp'),
cssnanos = require('gulp-cssnano'),
uglifyjs = require('gulp-uglify'),
mywatch = require('gulp-watch'),
mybrowsync = require('browser-sync'),
reload = mybrowsync.reload;


// css task
gulp.task('mini-css',function(){
gulp.src('app/css/*.css')
.pipe(cssnanos())
.pipe(gulp.dest('app/dist'))
.pipe(reload({stream:true}));
});


// uglify task
gulp.task('mini-js',function(){
gulp.src('app/js/*.js')
.pipe(uglifyjs())
.pipe(gulp.dest('app/dist'));
});


// html task
gulp.task('html',function(){
	gulp.src('*.html')
	.pipe(reload({stream:true}));
});

gulp.task('brcolor',function(){
	gulp.src('*.css')
	.pipe(reload({stream:true}));
});

//browser-sync task
gulp.task('browser-sync',function(){
	mybrowsync({
		server:{
			baseDir: ['./','./app/dist/*.css']
		}
	});
});


//watch task
gulp.task('watch-watch',function(){
	// gulp.watch('app/css/*.css',['mini-css']);
	gulp.watch('app/js/*.js',['mini-js']);
	gulp.watch('*.html',['html']);
});


//default task
gulp.task('default',['mini-css','mini-js','browser-sync','watch-watch']);
