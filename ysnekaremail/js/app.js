function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

 $(".thankYou").hide();
$("#hide").click(function(){
	$("#error").html('');
	
	if($("#email").val() === ''){
		$("#error").html("Please enter email");
	}else{

		if(validateEmail($("#email").val())){

			var request = $.ajax({
			  url: "contactform.php",
			  type: "POST",
			  data: {"email" : $("#email").val()},
			  success: function(data){
			     $("form").hide();
     			$(".thankYou").show();
			  }
			  
			});
			
		}
		else{
			$("#error").html("Please enter valid email");
		}
		
	}
	return false;
   
});


